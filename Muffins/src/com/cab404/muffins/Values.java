package com.cab404.muffins;

import com.badlogic.gdx.graphics.Color;

import java.util.ArrayList;

/**
 * @author cab404
 */
public class Values {
    public static float min_size, max_size;
    public static float min_speed, max_speed;
    public static float rotation_dispersion;
    public static float muffin_alpha;
    public static int max_muffin;
    public static boolean lsd = false;

    public static ArrayList<Color> colors = new ArrayList<>();
    public static Color bg;

    static {
        min_size = .1f;
        max_size = .5f;

        min_speed = 150f;
        max_speed = 300f;

        muffin_alpha = 0.5f;

        rotation_dispersion = 20f;

        max_muffin = 300;

        colors.add(Color.valueOf("dd67bc"));
        colors.add(Color.valueOf("4d5ac9"));
        colors.add(Color.valueOf("76c554"));
        colors.add(Color.valueOf("8a3686"));
        colors.add(Color.valueOf("902da9"));
        colors.add(Color.valueOf("14595f"));
        colors.add(Color.valueOf("75c3a5"));

        bg = Color.valueOf("c2c7d6");

    }

}
