package com.cab404.muffins;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

/**
 * @author cab404
 */
public class Muffin {
    private Sprite pic;
    private float speed, rot_speed;

    public static float getRandomFloat(float min, float max) {
        return (float) ((Math.random() * (max - min)) + min);
    }

    public Muffin(Texture texture) {

        pic = new Sprite(texture);
        speed = getRandomFloat(Values.min_speed, Values.max_speed);
        rot_speed = getRandomFloat(-Values.rotation_dispersion, Values.rotation_dispersion);

        float scale = getRandomFloat(Values.min_size, Values.max_size);
        pic.setSize(pic.getWidth() * scale, pic.getHeight() * scale);

        Color color;
        if (Values.lsd) {
            color = Values.colors.get((int) (Math.random() * Values.colors.size())).cpy();
        } else {
            color = Color.WHITE.cpy();
        }
        color.a = Values.muffin_alpha;

        pic.setColor(color);

    }

    public void setPosition(float x, float y) {
        pic.setPosition(x, y);
    }

    public float getWidth() {
        return pic.getTexture().getWidth();
    }

    public float getHeight() {
        return pic.getTexture().getHeight();
    }

    public void update(float time) {
        pic.setY(pic.getY() - speed * time);
        pic.rotate(rot_speed * time);
    }

    public boolean isEaten() {
        return (pic.getY() + getWidth() + getHeight()) < 0;
    }

    public void render(SpriteBatch batch) {
        pic.draw(batch);
    }

}
