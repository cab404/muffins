package com.cab404.muffins;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL11;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.ArrayList;
import java.util.Iterator;

public class Muffins implements ApplicationListener {
    private ArrayList<Muffin> muffins;
    private Texture muffin;
    private SpriteBatch batch;

    float w, h;

    @Override
    public void create() {
        muffins = new ArrayList<>();

        w = Gdx.graphics.getWidth();
        h = Gdx.graphics.getHeight();

        Texture.setEnforcePotImages(false);
        muffin = new Texture(Gdx.files.internal("data/muffin.png"));


    }

    @Override
    public void dispose() {
        muffins.clear();
        muffin.dispose();
        batch.dispose();
    }

    private void update() {
        if (muffins.size() < Values.max_muffin && Math.random() <= 0.05f) {
            Muffin muffin = new Muffin(this.muffin);
            muffin.setPosition(Muffin.getRandomFloat(0, w - muffin.getWidth() / 2), h);
            muffins.add(muffin);
        }

        Iterator<Muffin> muf = muffins.iterator();
        while (muf.hasNext()) {
            Muffin muffin = muf.next();
            muffin.update(Gdx.graphics.getDeltaTime());
            if (muffin.isEaten()) muf.remove();
        }
    }

    @Override
    public void render() {
        update();
        Gdx.gl.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
        Gdx.gl.glClearColor(Values.bg.r, Values.bg.g, Values.bg.b, Values.bg.a);

        batch.begin();
        for (Muffin muffin : muffins) muffin.render(batch);
        batch.end();

    }

    @Override
    public void resize(int width, int height) {
        w = width;
        h = height;

        batch = new SpriteBatch();
    }

    @Override
    public void pause() {
        //
    }

    @Override
    public void resume() {
        // Перезагружаем тексуры, ибо их наверняка диспознули.
        muffin = new Texture(Gdx.files.internal("data/muffin.png"));
    }
}
