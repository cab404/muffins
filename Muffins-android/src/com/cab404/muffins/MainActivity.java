package com.cab404.muffins;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.badlogic.gdx.backends.android.AndroidLiveWallpaperService;

public class MainActivity extends AndroidLiveWallpaperService {
    @Override
    public ApplicationListener createListener(boolean isPreview) {
        return new Muffins();
    }

    @Override
    public AndroidApplicationConfiguration createConfig() {

        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        return cfg;

    }

    @Override
    public void offsetChange(ApplicationListener listener, float xOffset, float yOffset, float xOffsetStep, float yOffsetStep, int xPixelOffset, int yPixelOffset) {
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }
}